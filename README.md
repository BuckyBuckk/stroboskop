# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://BuckyBuckk@bitbucket.org/BuckyBuckk/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/fb7a840e85668bd010fefab2e64f381ac67c25cc

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/0aec57e78ce44e76d014c8ca3c14d00f9b0acfff

Naloga 6.3.2:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/7c6530e9a97fd5f0b4bcf3ad74153c14d85a334f

Naloga 6.3.3:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/8ed93eb4a368a6dd1487c3cb60e7e6962d1006fa

Naloga 6.3.4:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/5b26fae532974ef1680e58390471405e8251f1a0

Naloga 6.3.5:

```
git checkout master
git merge izgled
```


## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/bee626d5d09944a8c15b484d581cc35c4607cf55

Naloga 6.4.2:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/1d5b39dccee063322f742876c3484261aaa74e0b

Naloga 6.4.3:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/9fdcb35eb513feb42176c08610224c53bf720063

Naloga 6.4.4:
https://bitbucket.org/BuckyBuckk/stroboskop/commits/f1553fb96ea9773031af4322fe614737be88c21d